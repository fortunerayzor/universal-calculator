﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;

namespace UniversalCalculator.Views
{
    public interface IViewBase<T> where T: ViewModels.ViewModelBase<T>, new()
    {
        T ViewModel { get; set; }
    }
}
