﻿using System;
using UniversalCalculator.Shared;
using Windows.System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace UniversalCalculator.Views
{
    public class VirtualKeyboardButton : Button, IVirtualKeyButton
    {
        public string KeyString
        {
            get { return (string)GetValue(KeyStringProperty); }
            set
            {
                SetValue(KeyStringProperty, value);
                VirtualKey key;
                if (Enum.TryParse(value, out key))
                {
                    Key = key;
                }
            }
        }

        public static readonly DependencyProperty KeyStringProperty =
            DependencyProperty.Register("KeyString", typeof(string), typeof(VirtualKeyboardButton), new PropertyMetadata(string.Empty));

        public VirtualKey? Key
        {
            get; private set;
        }
    }
}