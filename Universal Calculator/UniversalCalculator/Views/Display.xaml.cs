﻿using System;
using UniversalCalculator.ViewModels;
using Windows.UI.Xaml.Controls;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace UniversalCalculator.Views
{
    public sealed partial class Display : UserControl, IViewBase<CalculationViewModel>
    {
        public Display()
        {
            this.InitializeComponent();
        }

        public CalculationViewModel ViewModel { get; set; }
    }
}