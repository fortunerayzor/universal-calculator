﻿using System;
using System.Collections.Generic;
using System.Linq;
using UniversalCalculator.ViewModels;
using Windows.System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace UniversalCalculator.Views
{
    public sealed partial class Keyboard : UserControl, IViewBase<KeyboardViewModel>
    {
        private class AnimationItem
        {
            public Control Element { get; set; }
            public DateTime Timestamp { get; set; }
        }
        private DispatcherTimer animationTimer;

        private KeyboardViewModel viewModel;

        private Queue<AnimationItem> animationQueue;

        private const double animationSpan = 50d;

        private const double animationInterval = 16.6d;

        private readonly Dictionary<VirtualKey, Control> controlDictionary = new Dictionary<VirtualKey, Control>();

        public Keyboard()
        {
            this.InitializeComponent();
            this.ConstructButtonDictionary();
            animationTimer = new DispatcherTimer();
            animationTimer.Interval = TimeSpan.FromMilliseconds(animationInterval);
            animationTimer.Tick += AnimationTimer_Tick;
            animationQueue = new Queue<AnimationItem>();
        }

        private void ConstructButtonDictionary()
        {
            foreach (var item in gridButtons.Children.OfType<VirtualKeyboardButton>())
            {
                if (item.Key.HasValue)
                {
                    controlDictionary.Add(item.Key.Value, item);
                }
            }
        }

        private void AnimationTimer_Tick(object sender, object e)
        {
            while (animationQueue.Count > 0)
            {
                var item = animationQueue.Peek();
                if (DateTime.Now - item.Timestamp >= TimeSpan.FromMilliseconds(animationSpan))
                {
                    animationQueue.Dequeue();
                    VisualStateManager.GoToState(item.Element, "Normal", true);
                }
                else
                {
                    break;
                }
            }
            if (animationQueue.Count == 0)
            {
                animationTimer.Stop();
            }
        }

        public KeyboardViewModel ViewModel
        {
            get { return viewModel; }
            set
            {
                if (viewModel != null)
                {
                    viewModel.GlobalKeyboardClicked -= GlobalKeyboardClicked;
                }
                viewModel = value;
                viewModel.GlobalKeyboardClicked += GlobalKeyboardClicked;
            }
        }

        private void GlobalKeyboardClicked(object sender, KeyboardClickedEventArgs e)
        {
            Control ctrl;
            if (controlDictionary.TryGetValue(e.Key, out ctrl))
            {
                VisualStateManager.GoToState(ctrl, "Pressed", true);
                animationQueue.Enqueue(new AnimationItem { Element = ctrl, Timestamp = DateTime.Now });

                if (!animationTimer.IsEnabled)
                {
                    animationTimer.Start();
                }
            }
        }
    }
}