﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UniversalCalculator.Shared
{
    public interface ICalculatorOperation
    {
        Operation CalculatorOperation { get; }
    }
}
