﻿namespace UniversalCalculator.Shared
{
    public enum Operation
    {
        Number0,
        Number1,
        Number2,
        Number3,
        Number4,
        Number5,
        Number6,
        Number7,
        Number8,
        Number9,
        FloatingPoint,
        CaretLeft,
        CaretUp,
        CaretRight,
        CaretDown,
        Equals,
        Addition,
        Subtraction,
        Multiplication,
        Division,
        Fraction,
    }
}
