﻿using UniversalCalculator.ViewModels;
using Windows.UI.Core;
using Windows.UI.Xaml.Controls;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace UniversalCalculator
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private MainViewModel calculatorViewModel;

        public MainPage()
        {
            InitializeComponent();
            CoreWindow.GetForCurrentThread().KeyDown += (sender, args) => calculatorViewModel?.Keyboard.OnGlobalKeyDown(sender, args.VirtualKey);
        }
        
        public void SetMainViewModel(MainViewModel cvm)
        {
            this.calculatorViewModel = cvm;
            this.Content = cvm.ToUIElement();
            
        }
    }
}