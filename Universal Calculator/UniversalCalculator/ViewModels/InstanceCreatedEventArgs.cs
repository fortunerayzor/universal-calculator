﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UniversalCalculator.ViewModels
{
    public class InstanceCreatedEventArgs<T>: EventArgs
    {
        public InstanceCreatedEventArgs(T instance)
        {
            this.Instance = instance;
        }

        public T Instance { get; private set; }
    }

    public delegate void InstanceCreatedEventHandler<T>(object sender, InstanceCreatedEventArgs<T> e);
}
