﻿using System;
using UniversalCalculator.Shared;
using Windows.System;
using Windows.UI.Core;
using Windows.UI.Xaml;

namespace UniversalCalculator.ViewModels
{
    public sealed class KeyboardViewModel : ViewModelBase<KeyboardViewModel>
    {
        public event KeyboardClickedHandler AnyKeyboardClicked;

        public event KeyboardClickedHandler VirtualKeyboardClicked;

        public event KeyboardClickedHandler GlobalKeyboardClicked;
        

        private void OnAnyKeyDown(object sender, VirtualKey key)
        {
           AnyKeyboardClicked?.Invoke(sender, new KeyboardClickedEventArgs(key));
        }
        
        protected override void Initialization()
        {
            
        }

        public void OnVirtualKeyboardClick(object sender, RoutedEventArgs e)
        {
            var ctrl = sender as IVirtualKeyButton;
            if(ctrl != null && ctrl.Key.HasValue)
            {
                OnAnyKeyDown(sender, ctrl.Key.Value);
                VirtualKeyboardClicked?.Invoke(sender, new KeyboardClickedEventArgs(ctrl.Key.Value));
            }
        }

        public override UIElement ToUIElement()
        {
            throw new NotImplementedException();
        }

        public void OnGlobalKeyDown(CoreWindow sender, VirtualKey key)
        {

            OnAnyKeyDown(sender, key);
            GlobalKeyboardClicked?.Invoke(sender, new KeyboardClickedEventArgs(key));
        }
    }

    public delegate void KeyboardClickedHandler(object sender, KeyboardClickedEventArgs e);

    public class KeyboardClickedEventArgs : EventArgs
    {
        public KeyboardClickedEventArgs(VirtualKey key)
        {
            Key = key;
        }

        public VirtualKey Key { get; private set; }
    }
}