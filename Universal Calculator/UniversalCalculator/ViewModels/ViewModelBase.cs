﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace UniversalCalculator.ViewModels
{
    public abstract class ViewModelBase<T> : INotifyPropertyChanged where T : ViewModelBase<T>,  new()
    {
        protected ViewModelBase()
        {
            Initialization();
            InvokeInstaceCreated();
        }

        public static event InstanceCreatedEventHandler<T> InstanceCreated;

        public static T GetInstance()
        {
            return new T();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected abstract void Initialization();

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void InvokeInstaceCreated()
        {
            InstanceCreated?.Invoke(this, new InstanceCreatedEventArgs<T>((T)this));
        }

        public abstract UIElement ToUIElement();
    }
}