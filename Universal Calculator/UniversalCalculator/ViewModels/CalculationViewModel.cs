﻿using System;
using System.ComponentModel;
using UniversalCalculator.ViewModels.Math;
using Windows.UI.Xaml;

namespace UniversalCalculator.ViewModels
{
    public class CalculationViewModel : ViewModelBase<CalculationViewModel>
    {
        private TermBase _rootTerm;

        private decimal resultValue;

        public decimal ResultValue
        {
            get
            {
                return resultValue;
            }
            private set
            {
                resultValue = value;
                this.OnPropertyChanged();
            }
        }

        public TermBase RootTerm
        {
            get
            {
                return _rootTerm;
            }
            private set
            {
                _rootTerm = value;
                this.OnPropertyChanged();
            }
        }

        public TermBase SelectedTerm { get; set; }

        public void StartCalculation()
        {
            ResultValue = RootTerm.Evaluate();
        }

        public void InsertTerm(TermBase t)
        {
            if (SelectedTerm is EmptyTerm)
            {

            }
            else if (SelectedTerm is RootTerm)
            {
                ((RootTerm)SelectedTerm).ChildTerm = t;
            }
        }

        protected override void Initialization()
        {
            RootTerm = new RootTerm(this);
            SelectedTerm = RootTerm;
        }

        public override UIElement ToUIElement()
        {
            return new Views.Display() { ViewModel = this };
        }
    }
}