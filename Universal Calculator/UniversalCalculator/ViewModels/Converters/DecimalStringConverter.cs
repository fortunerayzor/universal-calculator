﻿using System;
using System.Globalization;
using Windows.UI.Xaml.Data;

namespace UniversalCalculator.ViewModels.Converters
{
    public class DecimalStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            if (value is decimal)
                return ((decimal)value).ToString();
            return string.Empty;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            if (value is decimal)
            {
                decimal t;
                if (decimal.TryParse((string)value, out t))
                    return t;
                return decimal.Zero;
            }
            return decimal.Zero;
        }
    }
}