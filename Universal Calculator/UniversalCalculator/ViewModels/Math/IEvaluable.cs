﻿namespace UniversalCalculator.ViewModels.Math
{
    public interface IEvaluable
    {
        decimal Evaluate();
    }
}