﻿using System.ComponentModel;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Input;

namespace UniversalCalculator.ViewModels.Math
{
    public abstract class TermBase : ContentControl, IEvaluable, INotifyPropertyChanged
    {
        protected readonly CalculationViewModel owner;

        public TermBase()
        {
            this.DataContext = this;
            this.GotFocus += TermBase_GotFocus;
            this.PointerPressed += TermBase_PointerPressed;
        }

        private void TermBase_PointerPressed(object sender, PointerRoutedEventArgs e)
        {
            e.Handled = true;
            this.Focus(FocusState.Programmatic);
        }

        public TermBase(CalculationViewModel owner) : this()
        {
            this.owner = owner;
        }

        public TermBase(TermBase parentTerm) : this()
        {
            this.ParentTerm = parentTerm;
            this.owner = parentTerm.owner;
        }

        public abstract event PropertyChangedEventHandler PropertyChanged;

        public TermBase ParentTerm { get; private set; }

        public abstract decimal Evaluate();

        public abstract void ReplaceChildWith(TermBase childTerm, TermBase newTerm);

        public abstract void ReplaceTermWith(TermBase newTerm);

        protected virtual TermBase GetLeftTerm()
        {
            return null;
        }

        protected virtual TermBase GetLowerTerm()
        {
            return null;
        }

        protected virtual TermBase GetRightTerm()
        {
            return null;
        }

        protected virtual TermBase GetUpperTerm()
        {
            return null;
        }
        private void TermBase_GotFocus(object sender, RoutedEventArgs e)
        {
            if (e.OriginalSource == sender)
            {
                owner.SelectedTerm = this;
            }
        }
    }
}