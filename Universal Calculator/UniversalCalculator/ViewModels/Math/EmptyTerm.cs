﻿using System;
using System.ComponentModel;
using Windows.UI.Xaml.Controls;

namespace UniversalCalculator.ViewModels.Math
{
    public class EmptyTerm : TermBase
    {
        public override event PropertyChangedEventHandler PropertyChanged;

        public override decimal Evaluate()
        {
            return decimal.Zero;
        }
        

        public override void ReplaceTermWith(TermBase newTerm)
        {
            ParentTerm.ReplaceChildWith(this, newTerm);
        }

        public override void ReplaceChildWith(TermBase childTerm, TermBase newTerm)
        {
            throw new InvalidOperationException($"{GetType().Name} has no children.");
        }

        public EmptyTerm(TermBase parent) : base(parent)
        {

        }
    }
}