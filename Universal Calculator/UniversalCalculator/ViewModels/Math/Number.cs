﻿using System;
using System.ComponentModel;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace UniversalCalculator.ViewModels.Math
{
    public sealed class Number : TermBase
    {
        public static readonly DependencyProperty ValueProperty =
            DependencyProperty.Register("Value", typeof(decimal), typeof(Number), new PropertyMetadata(0m));


        public Number(TermBase parent) : base(parent)
        {
        }

        public Number(TermBase parent, decimal d) : this(parent)
        {
            Value = d;
        }

        public override event PropertyChangedEventHandler PropertyChanged;

        public decimal Value
        {
            get
            {
                return (decimal)GetValue(ValueProperty);
            }
            set
            {
                SetValue(ValueProperty, value);
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Value)));
            }
        }

        public override decimal Evaluate()
        {
            return Value;
        }

        public override void ReplaceChildWith(TermBase childTerm, TermBase newTerm)
        {
            throw new InvalidOperationException($"{GetType().Name} has no children");
        }

        public override void ReplaceTermWith(TermBase newTerm)
        {
            ParentTerm.ReplaceChildWith(this, newTerm);
        }
    }
}