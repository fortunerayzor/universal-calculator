﻿using System;
using System.ComponentModel;
using Windows.UI.Xaml;

namespace UniversalCalculator.ViewModels.Math
{
    public sealed class Fraction : TermBase
    {
        public static readonly DependencyProperty DenominatorTermProperty =
            DependencyProperty.Register("Denominator", typeof(TermBase), typeof(Fraction), new PropertyMetadata(null));

        public static readonly DependencyProperty NumeratorProperty =
            DependencyProperty.Register("Numerator", typeof(TermBase), typeof(Fraction), new PropertyMetadata(null));

        public override event PropertyChangedEventHandler PropertyChanged;


        public TermBase Denominator
        {
            get { return (TermBase)GetValue(DenominatorTermProperty); }
            set
            {
                SetValue(DenominatorTermProperty, value);
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Denominator)));
            }
        }

        public TermBase Numerator
        {
            get { return (TermBase)GetValue(NumeratorProperty); }
            set
            {
                SetValue(NumeratorProperty, value);
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Numerator)));
            }
        }

        public Fraction(TermBase parent) : base(parent)
        {
            Numerator = new EmptyTerm(this);
            Denominator = new EmptyTerm(this);
        }

        public override decimal Evaluate()
        {
            return Numerator.Evaluate() / Denominator.Evaluate();
        }

        protected override TermBase GetUpperTerm()
        {
            return Numerator;
        }

        protected override TermBase GetLowerTerm()
        {
            return Denominator;
        }

        public override void ReplaceChildWith(TermBase childTerm, TermBase newTerm)
        {
            if (childTerm == Numerator)
            {
                Numerator = newTerm;
                owner.SelectedTerm = Numerator;
            }
            else if (childTerm == Denominator)
            {
                Denominator = newTerm;
                owner.SelectedTerm = Denominator;
            }
        }

        public override void ReplaceTermWith(TermBase newTerm)
        {
            ParentTerm.ReplaceChildWith(this, newTerm);
        }
    }
}