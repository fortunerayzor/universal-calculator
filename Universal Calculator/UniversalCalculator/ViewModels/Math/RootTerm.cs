﻿using System;
using System.ComponentModel;
using Windows.UI.Xaml;

namespace UniversalCalculator.ViewModels.Math
{
    internal class RootTerm : TermBase
    {
        // Using a DependencyProperty as the backing store for Child. This enables animation,
        // styling, binding, etc...
        public static readonly DependencyProperty ChildProperty =
            DependencyProperty.Register("ChildTerm", typeof(TermBase), typeof(RootTerm), new PropertyMetadata(null));

        public RootTerm(CalculationViewModel owner) : base(owner)
        {
        }

        public override event PropertyChangedEventHandler PropertyChanged;

        public TermBase ChildTerm
        {
            get
            {
                return (TermBase)GetValue(ChildProperty);
            }
            set
            {
                SetValue(ChildProperty, value);
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(ChildTerm)));
            }
        }

        public override decimal Evaluate()
        {
            return ChildTerm.Evaluate();
        }

        public override void ReplaceChildWith(TermBase childTerm, TermBase newTerm)
        {
            if(ChildTerm == childTerm)
            {

            }
        }

        public override void ReplaceTermWith(TermBase newTerm)
        {
            throw new InvalidOperationException($"{GetType().Name} cannot be replaced");
        }
    }
}