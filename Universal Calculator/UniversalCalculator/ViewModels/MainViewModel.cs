﻿using System;
using UniversalCalculator.ViewModels.Math;
using Windows.System;
using Windows.UI.Xaml;

namespace UniversalCalculator.ViewModels
{
    public class MainViewModel : ViewModelBase<MainViewModel>
    {
        private CalculationViewModel _display;
        private KeyboardViewModel _keyboard;

        public CalculationViewModel Display => _display;
        public KeyboardViewModel Keyboard => _keyboard;

        private TermBase selectedTerm => _display?.SelectedTerm;

        private void MoveCaret(CaretDirection direction)
        {
        }

        protected override void Initialization()
        {
            this._keyboard = KeyboardViewModel.GetInstance();
            this._display = CalculationViewModel.GetInstance();

            var fraction = new Fraction(_display.SelectedTerm);
            _display.InsertTerm(fraction);
            fraction.ReplaceChildWith(fraction.Numerator, new Number(fraction));

            _keyboard.AnyKeyboardClicked += AnyKeyboardClicked;
        }

        private void AnyKeyboardClicked(object sender, KeyboardClickedEventArgs e)
        {
            switch (e.Key)
            {
                case VirtualKey.Number0:
                case VirtualKey.NumberPad0: this.AppendNumber(0m); break;
                case VirtualKey.Number1:
                case VirtualKey.NumberPad1: this.AppendNumber(1m); break;
                case VirtualKey.Number2:
                case VirtualKey.NumberPad2: this.AppendNumber(2m); break;
                case VirtualKey.Number3:
                case VirtualKey.NumberPad3: this.AppendNumber(3m); break;
                case VirtualKey.Number4:
                case VirtualKey.NumberPad4: this.AppendNumber(4m); break;
                case VirtualKey.Number5:
                case VirtualKey.NumberPad5: this.AppendNumber(5m); break;
                case VirtualKey.Number6:
                case VirtualKey.NumberPad6: this.AppendNumber(6m); break;
                case VirtualKey.Number7:
                case VirtualKey.NumberPad7: this.AppendNumber(7m); break;
                case VirtualKey.Number8:
                case VirtualKey.NumberPad8: this.AppendNumber(8m); break;
                case VirtualKey.Number9:
                case VirtualKey.NumberPad9: this.AppendNumber(9m); break;
                case VirtualKey.Enter: this.Display.StartCalculation(); break;
                default:
                    break;
            }
        }

        public void FocusDefaultTerm()
        {
            this.selectedTerm.Focus(FocusState.Programmatic);
        }

        private void AppendNumber(decimal v)
        {
            var number = this.selectedTerm as Number;
            if (number != null)
            {
                number.Value = number.Value * 10 + v;
            }
            if (this.selectedTerm is EmptyTerm)
            {
                this.selectedTerm.ReplaceTermWith(new Number(this.selectedTerm.ParentTerm, v));
            }
        }

        public override UIElement ToUIElement()
        {
            return new Views.MainView() { ViewModel = this };
        }

        private enum CaretDirection
        {
            Left, Up, Right, Down
        }
    }
}